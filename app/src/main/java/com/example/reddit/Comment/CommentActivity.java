package com.example.reddit.Comment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.reddit.ExtractXML;
import com.example.reddit.FeedAPI;
import com.example.reddit.R;
import com.example.reddit.URLS;
import com.example.reddit.model.Feed;
import com.example.reddit.model.entry.Entry;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class CommentActivity extends AppCompatActivity {
    private static  final  String TAG = "Commentactivity";

    public static final String BASE_URL = "https://www.reddit.com/r/";

    private static String postURL;
    private static String postThumbnailURL;
    private static String postAuthor;
    private static String postTitle;
    private static String postUpdated;
    private  int defaultImage;


    private String currentFeed;

    private ListView mListView;

   private ArrayList<Comment> mComments;

    //private ProgressBar mProgressBar = (ProgressBar)findViewById(R.id.commentsprogressbar);
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coments);
        Log.d(TAG, "onCreate: starting :");
        //mProgressBar.setVisibility(View.VISIBLE);
        setupImageLoader();
        initPost();
        Log.d(TAG, "onCreate: reached--------------:");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        FeedAPI feedAPI = retrofit.create(FeedAPI.class);
        Call<Feed> call =feedAPI.getFeed(currentFeed);

        call.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                Log.d(TAG, "onResponse: Server Response----------: "+ response.toString());
                mComments = new ArrayList<>();
                List<Entry> entrys = response.body().getEntrys();
                for(int i = 0; i<entrys.size(); i++){
                    ExtractXML extract = new ExtractXML(entrys.get(i).getContent(),"<div class=\"md\"><p>","</p>");
                    List<String>commentDetails = extract.start();
                    //Log.d(TAG, "onResponse: reached here too ----------------------------");
                    //extract.start();
                    try{
                    mComments.add(new Comment(
                            commentDetails.get(0),
                            entrys.get(i).getAuthor().getName(),
                            entrys.get(i).getUpdated(),
                            entrys.get(i).getId()

                    ));
                   }
                   catch (IndexOutOfBoundsException e){
                       mComments.add(new Comment(
                               "Error reading comment",
                               "None",
                               "None",
                               "None"
                       ));
                       Log.d(TAG, "onResponse: IndexOutOfBoundsException: " +e.getMessage());
                   }
                       catch (NullPointerException e){
                           mComments.add(new Comment(
                                   commentDetails.get(0),
                                   "None",
                                   entrys.get(i).getUpdated(),
                                   entrys.get(i).getId()
                           ));
                       Log.d(TAG, "onResponse: NullPointerException: " +e.getMessage());
                   }
                }
                mListView = (ListView)findViewById(R.id.commentsListView);
                CommentsListAdapter adapter = new CommentsListAdapter(CommentActivity.this,R.layout.comments_layout,mComments);
                mListView.setAdapter(adapter);

               //mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                Log.e(TAG, "onFailure: Unable to retrieve RSS: "+ t.getMessage() );
                Toast.makeText(CommentActivity.this, "An error occured", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void initPost(){
        Intent incomingIntent = getIntent();
        postURL = incomingIntent.getStringExtra("post_url");
        postThumbnailURL = incomingIntent.getStringExtra("thumbnail_url");
        postAuthor = incomingIntent.getStringExtra("author");
        postTitle = incomingIntent.getStringExtra("title");
        postUpdated = incomingIntent.getStringExtra("updated");

        TextView title = (TextView)findViewById(R.id.postTitle);
        TextView author = (TextView)findViewById(R.id.postAuthor);
        TextView updated = (TextView)findViewById(R.id.postUpdated);
        ImageView thumbnail = (ImageView)findViewById(R.id.postThumbnail);
        //Button btnReply = (Button)findViewById(R.id.btnPostReply);
        ProgressBar progressBar =(ProgressBar)findViewById(R.id.postprogressbar);

        title.setText(postTitle);
        author.setText(postAuthor);
        updated.setText(postUpdated);
        displayImage(postThumbnailURL,thumbnail,progressBar);

        try {
            String[] splitURL = postURL.split(BASE_URL);
            currentFeed = splitURL[1];
            Log.d(TAG, "initPost: current feed: " + currentFeed);
        }catch (ArrayIndexOutOfBoundsException e){
            Log.e(TAG, "initPost: ArrayIndexOutOfBoundsException: "+ e.getMessage() );
        }

    }
    private  void  displayImage(String imageUrl, ImageView imageView, final  ProgressBar progressBar){
        ImageLoader imageLoader = ImageLoader.getInstance();


        //create display options
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(defaultImage)
                .showImageOnFail(defaultImage)
                .showImageOnLoading(defaultImage).build();

        //download and display image from url
        imageLoader.displayImage(imageUrl, imageView, options,  new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressBar.setVisibility(View.VISIBLE);
            }
            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                progressBar.setVisibility(View.GONE);
            }
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.GONE);
            }
            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                progressBar.setVisibility(View.GONE);
            }

        });

    }


    /**
     * Required for setting up the Universal Image loader Library
     */
    private void setupImageLoader(){
        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                CommentActivity.this)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP

         defaultImage = CommentActivity.this.getResources().getIdentifier("@drawable/image_failed",null,CommentActivity.this.getPackageName());

    }
}
