package com.example.reddit;

import com.example.reddit.model.Feed;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface FeedAPI {
    String BASE_URL = "https://www.reddit.com/r/";
    //Non static feed name
    @GET("{feed_name}/.rss")
    Call<Feed> getFeed(@Path("feed_name") String feed_name);

    @GET("popular/{trend_name}/.rss")
    Call<Feed> getTrend(@Path("trend_name") String trend_name);

    /*@GET("earthporn/.rss")
    Call<Feed> getFeed();*/
}
